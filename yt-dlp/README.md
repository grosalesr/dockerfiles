# yt-dlp

see [yt-dlp](https://github.com/yt-dlp/yt-dlp)

# Build

Make sure the Dockerfile is in the same directory before running the build command:

* Using provided script

`bash yt-dlp.sh build`

* Or by using docker/podman

`podman build -t localhost/yt-dlp:latest .`

# Execution

The script `yt-dlp.sh` is provided to simplify execution, it can build the container and run the application.

> `yt-dlp` is executed when the container is executed; all flags are passed as is. The provided script also exposes the same behaviour.

* files will be saved in `$HOME/Downloads/`
* enclose URL with double quotes, eg: `"URL"` to avoid terminal to interpret special characters in the URL.

**notice**: When using the container directly, make sure pass the volume where you want files to be saved. **In** the containers, files will be saved in `/tmp`


script examples:

* Extracts audio from URL

`bash yt-dlp.sh -x`

