#!/usr/bin/bash

build(){
  podman build -t localhost/yt-dlp:latest .
}

run() {
  # due to the way the container is build, the flags exposed are the same as
  # yt-dlp hence variables passed from the script are passed as is
  local args=$@
  local target="$HOME/Downloads"

  $container_engine run -it --rm \
    -v ${target}:/tmp:Z \
    localhost/yt-dlp:latest \
    $args
}

main() {
  # change to 'docker' if you don't use podman
  local container_engine="podman"

  if [ "$1" == "build" ]; then
    build
  else
    run $@
  fi
}

main $@
