#!/usr/env python3

import tkinter as tk

def pressed(name):
    print("Button Pressed: {}".format(name))
    #print(name)

mainPage = tk.Tk()

# Title
tk.Label(mainPage, text="SoftLayer VPN - Contained").pack()

# Options
options = ["MotionPro", "Firefox", "Remina"]
for option in options:
    btn = tk.Button(mainPage, text=option, command=lambda name=option:pressed(name))
    btn.pack(fill="x")


# Executes App
mainPage.mainloop()
